package ru.murat.sparkstreaming.producing

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.streaming.{OutputMode, Trigger}
import org.apache.spark.sql.types.StructType
import ru.murat.model.Book

object ProducerApp extends App {

  val spark = SparkSession
    .builder()
    .master("local[*]")
    .appName("WriterJob")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")
  val bookSchema = new StructType()
    .add("Name", "string")
    .add("Author", "string")
    .add("Rating", "double")
    .add("Reviews", "long")
    .add("Price", "double")
    .add("Year", "int")
    .add("Genre", "string")

  import spark.implicits._



 val readCSVDF =  spark.readStream
    .option("header",value = false)
    .option("inferSchema",value = true)
    .schema(bookSchema)
    .csv("src/main/resources/data")
    .withColumnRenamed("User Rating", "Rating")
    .filter("Rating is not null")
    .as[Book]
    .toJSON
    .selectExpr("CAST(value as STRING)")


  readCSVDF.writeStream
    .outputMode(OutputMode.Append()).trigger(Trigger.Once())
    .format("kafka")
    .option("kafka.bootstrap.servers", "localhost:29093")
    .option("topic", "books")
    .option("checkpointLocation","src/main.resources/producer-kafka-checkpoint")
    .start().awaitTermination()

  println("written to kafka from dataframe")
}

