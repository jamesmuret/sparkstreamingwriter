package ru.murat.sparkstreaming.consuming

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import ru.murat.model.Book;

object ConsumerApp extends App {

  val spark = SparkSession
    .builder()
    .master("local[*]")
    .appName("WriterJob")
    .getOrCreate()

  val userSchema = new StructType()
    .add("Name", "string")
    .add("Author", "string")
    .add("Rating", "double")
    .add("Reviews", "long")
    .add("Price", "double")
    .add("Year", "int")
    .add("Genre", "string")
  val checkpointPath = "/resources/consumer"
  import spark.implicits._

  /*
   val dfKafkaR =  spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers","localhost:29093")
      .option("topic",books")
      .load();

  val bookDF =   dfKafkaR
    .selectExpr("CAST(value as STRING)").
    filter(col("Rating") > 4);

    bookDF.write
    booksDF.writeStream
    .trigger(Trigger.Once)
    .format("parquet")
    .option("checkpointLocation", checkpointPath)
    .start(dataPath)
    .save("/resources
    .start()
    .awaitTermination()


*/
}

